# Graph API (JAVA) demo

This application can create and list calendar events of a user (in tenant) without logging in as any user. I.e. not using Delegated Permissions but Application permissions.

## Environment
OpenJDK 17
Gradle latest

## oAuth properties
- **app.id**: application (aka client) id
- **app.secret**: application password
- **app.tenantId**: the organization this client belongs to on Azure
- **app.user**: the user for whom we create event; this user belongs to tenantId

## Run
`./gradlew --console plain run` to run
